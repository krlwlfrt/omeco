import {suite, test} from '@testdeck/mocha';
import {strictEqual} from 'assert';
import {readFile} from 'fs/promises';
import {join} from 'path';
import {compilePlantUml} from '../src/compile-plantuml';
import {extractData} from '../src/extract';

@suite()
export class CompilePlantumlSpec {
  @test
  async 'compile puml'() {
    let buffer = await readFile(join(__dirname, 'resources', 'metadata.v2.xml'));

    const metadata = buffer.toString();

    const extractedData = await extractData(metadata);

    const compiledPuml = compilePlantUml(extractedData);

    buffer = await readFile(join(__dirname, 'resources', 'metadata.v2.puml'));

    strictEqual(compiledPuml, buffer.toString());
  }
}

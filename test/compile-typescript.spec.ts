import {Property} from '@krlwlfrt/tsg';
import {params, suite, test} from '@testdeck/mocha';
import {strictEqual, throws} from 'assert';
import {generatePropertyType} from '../src/compile-typescript';

@suite()
export class CompileTypescriptSpec {
  @test
  'translate TypeScript property throws for unknown types'() {
    throws(() => {
      generatePropertyType({
        type: {
          name: 'Foo',
          namespace: 'Edm',
        },
      } as Property);
    });
  }

  @params({
    input: {
      type: {
        name: 'Boolean',
        namespace: 'Edm',
      },
    },
    result: 'boolean',
  })
  @params({
    input: {
      type: {
        name: 'Null',
        namespace: 'Edm',
      },
    },
    result: 'null',
  })
  @params({
    input: {
      type: {
        name: 'Single',
        namespace: 'Edm',
      },
    },
    result: 'number',
  })
  @params({
    input: {
      type: {
        name: 'Sbyte',
        namespace: 'Edm',
      },
    },
    result: 'number',
  })
  @params({
    input: {
      type: {
        name: 'Int64',
        namespace: 'Edm',
      },
    },
    result: 'number',
  })
  @params({
    input: {
      type: {
        name: 'Int32',
        namespace: 'Edm',
      },
    },
    result: 'number',
  })
  @params({
    input: {
      type: {
        name: 'Int16',
        namespace: 'Edm',
      },
    },
    result: 'number',
  })
  @params({
    input: {
      type: {
        name: 'Double',
        namespace: 'Edm',
      },
    },
    result: 'number',
  })
  @params({
    input: {
      type: {
        name: 'Decimal',
        namespace: 'Edm',
      },
    },
    result: 'number',
  })
  @params({
    input: {
      type: {
        name: 'Time',
        namespace: 'Edm',
      },
    },
    result: 'string',
  })
  @params({
    input: {
      type: {
        name: 'String',
        namespace: 'Edm',
      },
    },
    result: 'string',
  })
  @params({
    input: {
      type: {
        name: 'Guid',
        namespace: 'Edm',
      },
    },
    result: 'string',
  })
  @params({
    input: {
      type: {
        name: 'DateTimeOffset',
        namespace: 'Edm',
      },
    },
    result: 'string',
  })
  @params({
    input: {
      type: {
        name: 'DateTime',
        namespace: 'Edm',
      },
    },
    result: 'string',
  })
  @params({
    input: {
      type: {
        name: 'Byte',
        namespace: 'Edm',
      },
    },
    result: 'string',
  })
  @params({
    input: {
      type: {
        name: 'Binary',
        namespace: 'Edm',
      },
    },
    result: 'string',
  })
  @params.naming((param) => `${param.input.type.namespace} ${param.input.type.name}`)
  'translate TypeScript property type'(param: { input: Property; result: string; }) {
    strictEqual(generatePropertyType(param.input), param.result);
  }
}

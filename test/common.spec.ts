import {ThingWithNameAndNamespace} from '@krlwlfrt/tsg';
import {params, suite} from '@testdeck/mocha';
import {strictEqual} from 'assert';
import {generateName, isString} from '../src/common';

@suite()
export class CommonSpec {
  @params({input: 'boolean', result: true})
  @params({input: 42, result: false})
  @params.naming((param) => param.input.toString())
  'is string'(param: { input: unknown, result: boolean }) {
    strictEqual(isString(param.input), param.result);
  }

  @params({
    input: {
      name: 'Bar',
      namespace: 'Foo',
    },
    // separator: '__',
    result: 'Foo__Bar',
  })
  @params({
    input: {
      name: 'Bar',
      namespace: 'Foo',
    },
    separator: '::',
    result: 'Foo::Bar',
  })
  @params.naming((param) => `${param.input.namespace} ${param.input.name}`)
  'generate name'(param: { input: ThingWithNameAndNamespace, separator: string, result: string; }) {
    strictEqual(generateName(param.input, param.separator), param.result);
  }
}

import {generateTypeScript} from '@krlwlfrt/tsg';
import {suite, test} from '@testdeck/mocha';
import {rejects, strictEqual} from 'assert';
import {readFile} from 'fs/promises';
import {join} from 'path';
import {generateName} from '../src/common';
import {generatePropertyType} from '../src/compile-typescript';
import {extractData} from '../src/extract';

@suite()
export class ExtractSpec {
  @test
  async 'v2'() {
    let buffer = await readFile(join(__dirname, 'resources', 'metadata.v2.xml'));
    const metadata = buffer.toString();
    buffer = await readFile(join(__dirname, 'resources', 'metadata.v2.d.ts'));
    strictEqual(generateTypeScript(await extractData(metadata), generateName, generatePropertyType), buffer.toString());
  }

  @test
  async 'v4'() {
    const buffer = await readFile(join(__dirname, 'resources', 'metadata.v4.xml'));
    const metadata = buffer.toString();

    return rejects(() => {
      return extractData(metadata);
    });
  }
}

/* eslint-disable */
/* tslint:disable */

export interface LoremIpsum__AbstractFoo {
  /**
   * @fixedLength false
   * @nullable false
   * @partOfKey true
   * @unicode false
   */
  AbstractKey: number;
}

export interface LoremIpsum__Foobar extends LoremIpsum__AbstractFoo {
  /**
   * @fixedLength false
   * @nullable false
   * @partOfKey true
   * @unicode false
   */
  Foo: number;
  /**
   * @fixedLength false
   * @nullable false
   * @partOfKey false
   * @unicode false
   */
  Bar: string;
  /**
   * @multiplicity *
   * @omeco__deferred true
   */
  Lorem: LoremIpsum__Ipsum[];
}

export interface LoremIpsum__Ipsum {
  /**
   * @fixedLength false
   * @nullable false
   * @partOfKey true
   * @unicode false
   */
  Foo: number;
  /**
   * @fixedLength false
   * @nullable true
   * @partOfKey false
   * @unicode false
   */
  Bar: string | null;
}

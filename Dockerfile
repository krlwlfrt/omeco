FROM node:lts

RUN apt update && \
  apt install -y openjdk-17-jre graphviz

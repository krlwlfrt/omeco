/*
 * Copyright (C) 2019, 2020 Karl-Philipp Wulfert
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * unknown WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */


export interface Metadata {
  'edmx:Edmx': {
    $: {
      Version: string;
      'xmlns:edmx': string;
    };
    'edmx:DataServices'?: DataService[];
  };
}

export interface DataService {
  $: {
    'm:DataServiceVersion': string;
    'xmlns:m': string;
  };

  Schema?: Schema[];
}

export interface Schema {
  $: {
    Namespace: string;
    [k: string]: string;
  };
  Association?: Association[];
  ComplexType?: EntityType[];
  EntityContainer?: EntityContainer[];
  EntityType?: EntityType[];
}

export interface EntityContainer {
  $: {
    Name: string;
    [k: string]: string;
  };
  AssociationSet: AssociationSet[];
  EntitySet: EntitySet[];
}

export interface EntitySet {
  $: {
    /**
     * NAMESPACE.NAME
     */
    EntityType: string;
    Name: string;
  };
}

export interface AssociationSet {
  $: {
    /**
     * NAMESPACE.NAME
     */
    Association: string;
    Name: string;
  };
  End: {
    $: {
      EntitySet: string;
      Role: string;
    };
  }[];
}

export interface EntityType {
  $: {
    BaseType?: string;
    Name: string;
  };
  Key?: Key[];
  NavigationProperty?: NavigationProperty[];
  Property?: Property[];
}

export interface Key {
  PropertyRef?: PropertyRef[];
}

export interface PropertyRef {
  $: {
    Name: string;
  };
}

export interface Property {
  $: {
    FixedLength?: 'false' | 'true';
    MaxLength?: string;
    Name: string;
    Nullable: 'false' | 'true';
    Precision?: string;
    Scale?: string;
    Type: string;
    Unicode?: 'false' | 'true';
    [k: string]: string | undefined;
  };
}

export interface NavigationProperty {
  $: {
    FromRole: string;
    Name: string;
    Relationship: string;
    ToRole: string;
  };
}

export interface End {
  $: {
    Multiplicity: string;
    Role: string;
    Type: string;
  };
}

export interface Principal {
  Principal: {
    $: {
      Role: string;
    };
    PropertyRef: PropertyRef[];
  }[];
}

export interface Dependent {
  Dependent: {
    $: {
      Role: string;
    };
    PropertyRef: PropertyRef[];
  }[];
}

export interface Association {
  $: {
    Name: string;
  };
  End: End[];
  ReferentialConstraint?: (Dependent | Principal)[];
}

/*
 * Copyright (C) 2019, 2020 Karl-Philipp Wulfert
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * unknown WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import { generateTypeScript } from '@krlwlfrt/tsg';
import { Command } from 'commander';
import { existsSync, readFileSync } from 'fs';
import { readFile, writeFile } from 'fs/promises';
import { basename, dirname, join, resolve } from 'path';
import { asyncParseString } from './async';
import { generateName, INDENTATION } from './common';
import { compilePlantUml, writePlantUmlPng } from './compile-plantuml';
import { generatePropertyType } from './compile-typescript';
import { extractData } from './extract';

// read and parse package file
const pkgJson = JSON.parse(
  readFileSync(join(__dirname, '..', 'package.json'))
    .toString(),
);

const commander = new Command('omeco');

commander
  .version(pkgJson.version) // set version of cli
  .allowUnknownOption(false); // disallow unknown options

commander
  .command('convert <metadataXml> [targetDir]')
  .description('OData metadata converter')
  .option('-f, --force', 'Whether or not to replace existing files in target directory')
  .option('-s, --sort', 'Whether or not to sort entities and properties by name')
  .option('-d, --debug', 'Whether or not to output debug files')
  .action(async (metadataXml, relativeTargetDir, cmd) => {
    // check if supplied metadata file exists
    if (!existsSync(metadataXml)) {
      throw new Error(`File '${metadataXml}' does not exist!`);
    }

    // set default value for targetDir
    let targetDir = dirname(metadataXml);
    if (typeof targetDir !== 'undefined') {
      targetDir = resolve(relativeTargetDir);
    }

    // set target file name
    const targetFileName = join(targetDir, basename(metadataXml, '.xml'));

    // check if files in target directory exist
    [
      '.d.ts',
      '.puml',
      '.png',
    ].forEach((extension) => {
      const existingFile = targetFileName + extension;
      if (existsSync(existingFile) && !cmd.force) {
        throw new Error(`File ${existingFile} does exist! Use option '-f' to replace existing files.`);
      }
    });

    // read metadata file
    const buffer = await readFile(metadataXml);

    // extract data
    const extractedData = await extractData(buffer.toString());

    // output debug information if desired
    if (cmd.debug) {
      const convertedMetadata = await asyncParseString(buffer.toString());
      await writeFile(`${targetFileName}.json`, JSON.stringify(convertedMetadata, null, INDENTATION));
      await writeFile(`${targetFileName}.extracted.json`, JSON.stringify(extractedData, null, INDENTATION));
    }

    // sort entities and properties by name
    if (cmd.sort) {
      extractedData.sort((a, b) => {
        return a.name.localeCompare(b.name);
      });

      extractedData.forEach((entity) => {
        entity.properties.sort((a, b) => {
          return a.name.localeCompare(b.name);
        });
      });
    }

    const extractedDataCopy = [...extractedData];

    // generate and compile TypeScript interfaces
    await writeFile(`${targetFileName}.d.ts`, generateTypeScript(
      extractedData,
      generateName,
      generatePropertyType,
    ));

    // generate and compile PlantUML description
    await writeFile(`${targetFileName}.puml`, compilePlantUml(extractedDataCopy));

    // generate PNG from PlantUML
    writePlantUmlPng(`${targetFileName}.puml`, `${targetFileName}.png`);
  });

commander
  .parse(process.argv);

// eslint-disable-next-line no-magic-numbers
if (commander.args.length < 1) {
  commander.help();
}

/*
 * Copyright (C) 2019, 2020 Karl-Philipp Wulfert
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * unknown WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import { Entity } from '@krlwlfrt/tsg';
import { createWriteStream } from 'fs';
import { generateName } from './common';

/**
 * Compile entities to a PlantUML description
 * @param entities List of entities to compile
 * @param rootNamespaces List of root namespaces
 * @returns PlantUML code
 */
export function compilePlantUml(entities: Entity[], rootNamespaces = ['Edm']): string {
  let output = '@startuml\nset namespaceSeparator ::\n';

  let extensions = '';

  let associations = '';

  for (const entity of entities) {
    output += `\nclass ${generateName(entity, '::')} {\n`;

    if (typeof entity.parent !== 'undefined') {
      extensions += `${generateName(entity.parent, '::')} <|-- ${generateName(entity, '::')}\n`;
    }

    for (const property of entity.properties) {
      let multiplicity = typeof property.multiple === 'boolean' && property.multiple ? 'n' : '1';

      if (Array.isArray(property.attributes)) {
        for (const attribute of property.attributes) {
          if (attribute.name === 'multiplicity' && typeof attribute.value === 'string') {
            multiplicity = attribute.value;
          }
        }
      }

      if (typeof property.type.namespace === 'string' && rootNamespaces.includes(property.type.namespace)) {
        output += `  ${property.name}:${property.type.name}\n`;
      } else {
        output += `  #${property.name}\n`;

        associations += `${generateName(entity, '::')}::${property.name} "1" --> "${multiplicity}" ${generateName(property.type, '::')} : ${property.name}\n`;
      }
    }

    output += '}\n';
  }

  if (extensions.length > 0) {
    output += `\n${extensions}`;
  }

  if (associations.length > 0) {
    output += `\n${associations}`;
  }

  output += '\n@enduml\n';

  return output;
}

/**
 * Write class diagram file (PNG) from PlantUML file
 * @param sourceFile Source file to read PlantUML from
 * @param targetFile Target file to write class diagram (PNG)
 */
export function writePlantUmlPng(sourceFile: string, targetFile: string): void {
  // eslint-disable-next-line @typescript-eslint/no-require-imports
  const plantuml = require('node-plantuml');
  const gen = plantuml.generate(sourceFile);
  gen.out.pipe(createWriteStream(targetFile));
}

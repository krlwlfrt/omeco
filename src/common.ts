/*
 * Copyright (C) 2019, 2020 Karl-Philipp Wulfert
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * unknown WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import { ThingWithNameAndNamespace } from '@krlwlfrt/tsg';

export const INDENTATION = 2;

/**
 * Generate name for a thing
 * @param thing Thing to generate name for
 * @param separator Separator for namespace and name
 * @returns A name for a thing
 */
export function generateName(thing: ThingWithNameAndNamespace, separator = '__'): string {
  if (typeof thing.namespace === 'undefined' || thing.namespace === 'Edm') {
    return thing.name;
  }

  return `${thing.namespace
    .split('.')
    .join('__')}${separator}${thing.name}`;
}

/**
 * Check whether something is a string or not
 * @param something Something to check
 * @returns True, if something is a string
 */
export function isString(something: unknown): something is string {
  return typeof something === 'string';
}

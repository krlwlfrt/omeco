/*
 * Copyright (C) 2019, 2020 Karl-Philipp Wulfert
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * unknown WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import { Attribute, Entity } from '@krlwlfrt/tsg';
import { asyncParseString } from './async';
import { isString } from './common';
import { EntityType, Metadata } from './metadata.v2';

/**
 * List of known properties
 */
const knownAttributes = [
  'FixedLength',
  'MaxLength',
  'Name',
  'Type',
  'Nullable',
  'Precision',
  'Scale',
  'Unicode',
];

/**
 * Extract entities and their properties from OData metadata
 * @param metadataString Metadata to extract entities from
 * @returns Promise that resolve with an Array of Entities
 */
export async function extractData(metadataString: string): Promise<Entity[]> {
  const metadata = await asyncParseString(metadataString) as Metadata;

  // get metadata version
  const version = metadata['edmx:Edmx'].$.Version;

  if (version !== '1.0') {
    throw new Error(`Metadata version '${version} not supported.`);
  }

  return extractDataV2(metadata);
}

/**
 * Get name and namespace from dotted identifier
 * @param identifier Dotted identifier to get name and namespace from
 * @returns Name and namespace
 */
export function getNameAndNamespace(identifier: string): { name: string, namespace: string; } {
  const parts = identifier.split('.');

  return {
    // eslint-disable-next-line no-magic-numbers
    name: parts[parts.length - 1],
    namespace: parts
      // eslint-disable-next-line no-magic-numbers
      .slice(0, parts.length - 1)
      .join('.'),
  };
}

/**
 * Extract entities from OData metadata version 2
 * @param metadata Metadata to extract entities from
 * @returns Array of entities
 */
export function extractDataV2(metadata: Metadata): Entity[] {
  const entities: Entity[] = [];

  if (!Array.isArray(metadata['edmx:Edmx']['edmx:DataServices'])) {
    return entities;
  }

  // iterate over data services
  for (const dataService of metadata['edmx:Edmx']['edmx:DataServices']) {
    if (!Array.isArray(dataService.Schema)) {
      // continue if data service doesn't contain schemas
      continue;
    }

    // iterate over contained schemas
    for (const schema of dataService.Schema) {
      const entityTypes: EntityType[] = [];

      for (const key in schema) {
        if (!Object.prototype.hasOwnProperty.call(schema, key) || key === '$') {
          continue;
        }

        if (!['ComplexType', 'EntityType', 'Association'].includes(key)) {
          console.warn(`Schema with namespace ${schema.$.Namespace} has unhandled item ${key}.`);
        }
      }

      if (Array.isArray(schema.ComplexType)) {
        entityTypes.push(...schema.ComplexType);
      }
      if (Array.isArray(schema.EntityType)) {
        entityTypes.push(...schema.EntityType);
      }

      // iterate over entity types
      for (const entityType of entityTypes) {
        // create a new interface
        const entity: Entity = {
          name: entityType.$.Name,
          namespace: schema.$.Namespace,
          properties: [],
        };

        const keyParts: string[] = [];
        if (Array.isArray(entityType.Key)) {
          for (const key of entityType.Key) {
            if (!Array.isArray(key.PropertyRef)) {
              continue;
            }

            for (const propertyRef of key.PropertyRef) {
              keyParts.push(propertyRef.$.Name);
            }
          }
        }

        // add interface to list of interfaces
        entities.push(entity);

        // set parent for interface if entity type has base type
        if (typeof entityType.$.BaseType !== 'undefined' && isString(entityType.$.BaseType)) {
          entity.parent = getNameAndNamespace(entityType.$.BaseType);
        }

        if (Array.isArray(entityType.Property)) {
          // iterate over properties
          for (const property of entityType.Property) {
            const attributes: Attribute[] = [];

            for (const attribute in property.$) {
              if (knownAttributes.includes(attribute)) {
                // noop
              } else if (attribute.indexOf(':') >= 0) {
                const parts = attribute.split(':');

                attributes.push({
                  // eslint-disable-next-line no-magic-numbers
                  name: parts[1],
                  namespace: parts[0],
                  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                  value: property.$[attribute]!,
                });
              } else {
                console.warn(`Ignoring ${schema.$.Namespace} > ${entityType.$.Name} > ${property.$.Name} > ${attribute}...`);
              }
            }

            attributes.push({
              name: 'fixedLength',
              namespace: 'Edm',
              value: property.$.FixedLength === 'true',
            });

            if (typeof property.$.MaxLength !== 'undefined') {
              attributes.push({
                name: 'maxLength',
                namespace: 'Edm',
                value: parseInt(property.$.MaxLength, 10),
              });
            }

            attributes.push({
              name: 'nullable',
              namespace: 'Edm',
              value: property.$.Nullable === 'true',
            });

            attributes.push({
              name: 'partOfKey',
              namespace: 'Edm',
              value: keyParts.indexOf(property.$.Name) >= 0,
            });

            if (typeof property.$.Precision !== 'undefined') {
              attributes.push({
                name: 'precision',
                namespace: 'Edm',
                value: parseInt(property.$.Precision, 10),
              });
            }

            if (typeof property.$.Scale !== 'undefined') {
              attributes.push({
                name: 'scale',
                namespace: 'Edm',
                value: parseInt(property.$.Scale, 10),
              });
            }

            attributes.push({
              name: 'unicode',
              namespace: 'Edm',
              value: property.$.Unicode === 'true',
            });

            entity.properties.push({
              attributes,
              multiple: /^Collection\(/.test(property.$.Type),
              name: property.$.Name,
              namespace: schema.$.Namespace,
              required: true,
              type: getNameAndNamespace(property.$.Type
                .replace(/^Collection\(/, '')
                .replace(/\)$/, '')),
            });
          }
        }

        // return if schema doesn't contain navigation properties
        if (!Array.isArray(entityType.NavigationProperty)) {
          continue;
        }

        for (const navigationProperty of entityType.NavigationProperty) {
          const associationNameAndNamespace = getNameAndNamespace(navigationProperty.$.Relationship);
          const associationName = associationNameAndNamespace.name;
          const associationNamespace = associationNameAndNamespace.namespace;

          const relevantSchema = dataService.Schema
            .find((possibleSchema) => possibleSchema.$.Namespace === associationNamespace);

          if (typeof relevantSchema === 'undefined') {
            console.warn(`Relevant schema with namespace '${associationNamespace}' could not be found.`);

            continue;
          }

          if (!Array.isArray(relevantSchema.Association)) {
            console.warn(`Relevant schema with namespace '${associationNamespace}' has no associations.`);

            continue;
          }

          const relevantAssociation = relevantSchema.Association
            .find((association) => associationName === association.$.Name);

          if (typeof relevantAssociation === 'undefined') {
            console.warn(`Association '${associationNamespace}.${associationName}' not found!`);
            continue;
          }

          const relevantEnd = relevantAssociation.End.find((end) => end.$.Role === navigationProperty.$.ToRole);

          if (typeof relevantEnd === 'undefined') {
            console.warn(`Relevant end '${navigationProperty.$.ToRole}' not found!`);
            continue;
          }

          entity.properties.push({
            attributes: [
              {
                name: 'multiplicity',
                namespace: 'Edm',
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                value: relevantEnd!.$.Multiplicity,
              },
              {
                name: 'deferred',
                namespace: 'omeco',
                value: true,
              },
            ],
            name: navigationProperty.$.Name,
            namespace: schema.$.Namespace,
            required: true,
            type: getNameAndNamespace(relevantEnd.$.Type),
          });
        }
      }
    }
  }

  return entities;
}

/*
 * Copyright (C) 2019, 2020 Karl-Philipp Wulfert
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * unknown WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import { Property } from '@krlwlfrt/tsg';
import { generateName } from './common';

/**
 * Translate an OData type to a TypeScript type
 * @param property Property to translate
 * @returns Type for Property
 */
export function generatePropertyType(property: Property): string {
  let typeScriptType = '';

  let multiplicity;
  let nullable = false;

  if (Array.isArray(property.attributes)) {
    for (const attribute of property.attributes) {
      if (attribute.name === 'multiplicity' && typeof attribute.value === 'string') {
        multiplicity = attribute.value;
      }

      if (attribute.name === 'nullable' && typeof attribute.value === 'boolean') {
        nullable = attribute.value;
      }
    }
  }

  if (property.type.namespace === 'Edm') {
    switch (property.type.name) {
      case 'Binary':
      case 'Byte':
      case 'DateTime':
      case 'DateTimeOffset':
      case 'Guid':
      case 'String':
      case 'Time':
        typeScriptType = 'string';
        break;
      case 'Decimal':
      case 'Double':
      case 'Int16':
      case 'Int32':
      case 'Int64':
      case 'Sbyte':
      case 'Single':
        typeScriptType = 'number';
        break;
      case 'Boolean':
        typeScriptType = 'boolean';
        break;
      case 'Null':
        typeScriptType = 'null';
        nullable = false;
        break;
      default:
        throw new Error(`No translation for ${property}.`);
    }
  } else {
    typeScriptType = generateName(property.type);

    if (typeof multiplicity !== 'undefined' && ['*', '0..n', '1..n'].indexOf(multiplicity) >= 0) {
      typeScriptType += '[]';
    }
  }

  if (nullable) {
    typeScriptType += ' | null';
  }

  return typeScriptType;
}

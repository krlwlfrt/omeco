# @krlwlfrt/omeco

[![pipeline status](https://img.shields.io/gitlab/pipeline/krlwlfrt/omeco.svg?style=flat-square)](https://gitlab.com/krlwlfrt/omeco/commits/master) 
[![npm](https://img.shields.io/npm/v/@krlwlfrt/omeco.svg?style=flat-square)](https://npmjs.com/package/@krlwlfrt/omeco)
[![license)](https://img.shields.io/npm/l/@krlwlfrt/omeco.svg?style=flat-square)](https://opensource.org/licenses/MIT)
[![documentation](https://img.shields.io/badge/documentation-online-blue.svg?style=flat-square)](https://krlwlfrt.gitlab.io/omeco)


This tool can extract data from an OData metadata file (currently only v2) and compile it to other representations of the extracted data. Currently these are:

* TypeScript interfaces
* PlantUML and a resulting class diagram
  * ***Caution***: Please make sure that `node-plantuml` is installed if you want to generate class diagrams.

## CLI

Install and execute `@krlwlfrt/omeco` globally and use it as a CLI.

```shell
npm install -g @krlwlfrt/omeco

omeco convert PATH/TO/METADATA.XML
```

This will generate output files in the same directory as the supplied metadata file with names which match the basename (without extension) of the source file.

For further usage refer to the help section of the CLI:

```shell
omeco --help
```

## Programmatically

Install `@krlwlfrt/omeco` as a dependency in your project and use it programmatically.

```shell
npm install @krlwlfrt/omeco
```

### Extract

Use `extractData` to extract data from a metadata description.

### Compile

* Use `generateTypeScript` from `@krlwlfrt/tsg` to generate TypeScript interfaces from the extracted data.
* Use `compilePlantUml` to generate PlantUML from the extracted data.
* Use `writePlantUmlPng` to write a class diagram (PNG) from generated PlantUML. 

## Documentation

[See documentation](https://krlwlfrt.gitlab.io/omeco/) for detailed description of usage. 

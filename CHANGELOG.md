# [1.1.0](https://gitlab.com/krlwlfrt/omeco/compare/v1.0.1...v1.1.0) (2024-11-25)



## [1.0.1](https://gitlab.com/krlwlfrt/omeco/compare/v1.0.0...v1.0.1) (2023-02-28)



# [1.0.0](https://gitlab.com/krlwlfrt/omeco/compare/v0.6.0...v1.0.0) (2023-02-28)



# [0.6.0](https://gitlab.com/krlwlfrt/omeco/compare/v0.5.0...v0.6.0) (2022-09-29)



# [0.5.0](https://gitlab.com/krlwlfrt/omeco/compare/v0.4.1...v0.5.0) (2021-07-26)



## [0.4.1](https://gitlab.com/krlwlfrt/omeco/compare/v0.4.0...v0.4.1) (2021-02-22)



# [0.4.0](https://gitlab.com/krlwlfrt/omeco/compare/v0.3.0...v0.4.0) (2021-02-22)



# [0.3.0](https://gitlab.com/krlwlfrt/omeco/compare/v0.2.0...v0.3.0) (2020-05-15)


### Features

* correctly handle namespaced schemas ([3184a6a](https://gitlab.com/krlwlfrt/omeco/commit/3184a6a8a9345d3304aca8bbbca9b55113e47410))



# [0.2.0](https://gitlab.com/krlwlfrt/omeco/compare/v0.1.0...v0.2.0) (2019-08-08)



# [0.1.0](https://gitlab.com/krlwlfrt/omeco/compare/v0.0.1...v0.1.0) (2019-05-02)



## [0.0.1](https://gitlab.com/krlwlfrt/omeco/compare/04a0101a00bf37a0ec1b4bd9319b58c5b9584c21...v0.0.1) (2019-04-16)


### Features

* add implementation ([04a0101](https://gitlab.com/krlwlfrt/omeco/commit/04a0101a00bf37a0ec1b4bd9319b58c5b9584c21))



